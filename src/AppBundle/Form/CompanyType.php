<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Entity\Company;

class CompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array('label'=>false, 'attr' => array('placeholder'=>"Name")))
            ->add('country', ChoiceType::class, array('label'=>false, 
            'choices'         => Company::getCountries(),
            'multiple'            => false,
            'expanded'            => false,
            'required'            => true,
            'attr' => array('class' => 'crs-country', 'data-region-id' => 'reg', 'data-default-value' => 'Canada', 'data-show-default-option' => 'false')
            ))
            ->add('province', ChoiceType::class, array('label'=>false, 
            'choices'         => Company::getRegions(),
            'multiple'            => false,
            'expanded'            => false,
            'required'            => true,
            'attr' => array('class' => 'reg', 'data-default-value' => 'Saskatchewan', 'data-show-default-option' => 'false')
            ))
            ->add('city', null, array('label'=>false, 'attr' => array('placeholder'=>"City")))
            ->add('postalCode', null, array('label'=>false, 'attr' => array('placeholder'=>"Postal Code", "title"=>"Accepted Values: A-Z, 0-9, -, spaces")))
            ->add('street', null, array('label'=>false, 'attr' => array('placeholder'=>"Street")))
            ->add('phone', null, array('required'=>false, 'label'=>false, 'attr' => array('placeholder'=>"Phone", "title"=>"Accepted Values: 0-9")))
            ->add('website', null, array('label'=>false, 'attr' => array('placeholder'=>"Website")))
            ->add('description', null, array('label'=>false, 'attr' => array('placeholder'=>"Description")));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_company';
    }


}

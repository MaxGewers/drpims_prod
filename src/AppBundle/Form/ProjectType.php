<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projectName')
            ->add('description', TextareaType::class, array('required' => false))
            ->add('startDate', DateType::class, array('widget' => 'single_text', 'data' => new DateTime('now')))
            ->add('endDate', DateType::class, array('widget' => 'single_text', 'required' => false))
            ->add('budget', MoneyType::class, array('data' => 0.00))
            ->add('status', ChoiceType::class, array('choices' => array('Pre-Project' => 'Pre-Project', 'Proposal' => 'Proposal',
            'Submitted' => 'Submitted', 'Accepted' => 'Accepted', 'In Progress' => 'In Progress', 
            'Completed' => 'Completed', 'On Hold' => 'On Hold' )))
            ->add('keywords')
            ->add('media');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Project'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_project';
    }


}

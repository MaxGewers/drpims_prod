<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')->add('lastName')->add('phoneNumber')->add('description')->add('email');
    }

    //Uncomment for story that adds researcher details
    //public function buildForm(FormBuilderInterface $builder, array $options)
    //{
    //    $builder->add('firstName')->add('lastName')->add('phoneNumber')->add('description')->add('email')->add('availability')->add('specialty')->add('specialtyDescription')->add('skill')->add('skillDescription')->add('researcherLevel')->add('researcherPayCategory')->add('userID');
    //}

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_person';
    }


}

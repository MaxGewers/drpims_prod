<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 * @UniqueEntity("name")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     *
     * @Assert\NotBlank( message = "Please fill out Company Name field" )
     * @Assert\Length( min = 1, max = 255,
     *                  minMessage = "Please lengthen Company Name to {{ limit }} characters or more",
     *                  maxMessage = "Please shorten Company Name to {{ limit }} characters or less" )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     *
     * @Assert\NotBlank( message = "Please fill out Country field" )
     * @Assert\Choice(callback = "getCountries",
     *                  message="{value}")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="province", type="string", length=255)
     *
     * @Assert\NotBlank( message = "Please fill out Province field" )
     * @Assert\Choice(callback = "getRegions")
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     *
     * @Assert\Length( max = 255,
     *                 maxMessage = "Please shorten City to {{ limit }} characters or less" )
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=20, nullable=true)
     *
     * @Assert\Length( max = 10,
     *                 maxMessage = "Please shorten Postal Code to {{ limit }} characters or less" )
     * @Assert\Regex( pattern="/^[\w\d -]+$/i",
     *                htmlPattern="[\w\d -]+",
     *                message = "Accepted values for Postal Code: 0-9, A-Z, -, spaces" )
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     *
     * @Assert\Length( max = 255,
     *                 maxMessage = "Please shorten Street to {{ limit }} characters or less" )
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     *
     * @Assert\Length( min = 10, max = 15,
     *                 minMessage = "Please lengthen Phone to {{ limit }} characters",
     *                 maxMessage = "Please shorten Phone to {{ limit }} characters" )
     *  @Assert\Regex( pattern="/^[\d]+$/",
     *                 htmlPattern="^[\d]+$",
     *                 message = "Accepted values for Phone: 0-9" )
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     *
     * @Assert\Regex( pattern="/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,63}(:[0-9]{1,5})?(\/.*)?$/i",
     *                htmlPattern="^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,63}(:[0-9]{1,5})?(\/.*)?$",
     *                message = "Please enter a valid URL in the Website field" )
     *  @Assert\Length( max = 255,
     *                 maxMessage = "Please shorten the Website URL to {{ limit }} characters or less" )
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, length=1000)
     *
     * @Assert\Length( max = 1000,
     *                 maxMessage = "Please shorten Description to {{ limit }} characters or less" )
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Company
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set province
     *
     * @param string $province
     *
     * @return Company
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Company
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Company
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Company
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public static function getRegions(){
        return array("Alberta"=>"Alberta","British Columbia"=>"British Columbia","Manitoba"=>"Manitoba","New Brunswick"=>"New Brunswick","Newfoundland and Labrador"=>"Newfoundland and Labrador","Northwest Territories"=>"Northwest Territories","Nova Scotia"=>"Nova Scotia","Nunavut"=>"Nunavut","Ontario"=>"Ontario","Prince Edward Island"=>"Prince Edward Island","Quebec"=>"Quebec","Saskatchewan"=>"Saskatchewan","Yukon"=>"Yukon","Alaska"=>"Alaska","American Samoa"=>"American Samoa","Arizona"=>"Arizona","Arkansas"=>"Arkansas","California"=>"California","Colorado"=>"Colorado","Connecticut"=>"Connecticut","Delaware"=>"Delaware","District of Columbia"=>"District of Columbia","Florida"=>"Florida","Georgia"=>"Georgia","Guam"=>"Guam","Hawaii"=>"Hawaii","Idaho"=>"Idaho","Illinois"=>"Illinois","Indiana"=>"Indiana","Iowa"=>"Iowa","Kansas"=>"Kansas","Kentucky"=>"Kentucky","Louisiana"=>"Louisiana","Maine"=>"Maine","Maryland"=>"Maryland","Massachusetts"=>"Massachusetts","Michigan"=>"Michigan","Minnesota"=>"Minnesota","Mississippi"=>"Mississippi","Missouri"=>"Missouri","Montana"=>"Montana","Nebraska"=>"Nebraska","Nevada"=>"Nevada","New Hampshire"=>"New Hampshire","New Jersey"=>"New Jersey","New Mexico"=>"New Mexico","New York"=>"New York","North Carolina"=>"North Carolina","North Dakota"=>"North Dakota","Northern Mariana Islands"=>"Northern Mariana Islands","Ohio"=>"Ohio","Oklahoma"=>"Oklahoma","Oregon"=>"Oregon","Pennsylvania"=>"Pennsylvania","Puerto Rico"=>"Puerto Rico","Rhode Island"=>"Rhode Island","South Carolina"=>"South Carolina","South Dakota"=>"South Dakota","Tennessee"=>"Tennessee","Texas"=>"Texas","Utah"=>"Utah","Vermont"=>"Vermont","Virgin Islands"=>"Virgin Islands","Virginia"=>"Virginia","Washington"=>"Washington","West Virginia"=>"West Virginia","Wisconsin"=>"Wisconsin","Wyoming"=>"Wyoming");
    }

    public static function getCountries(){
        return array("Canada"=>"Canada","United States"=>"United States");
    }

    public function __toString()
    {
        return 'Name: ' . $this->getName() . ', ID: ' . $this->getId();
    }
}


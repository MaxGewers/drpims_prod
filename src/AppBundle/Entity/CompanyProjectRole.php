<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CompanyProjectRole
 *
 * @ORM\Table(name="company_project_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyProjectRoleRepository")
 */
class CompanyProjectRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="companyID", referencedColumnName="id")
     * @Assert\NotNull( message = "Please select a valid company from the drop-down list" )
     */
    private $companyID;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="projectID", referencedColumnName="projectID")
     * @Assert\NotNull( message = "Please select a valid project from the drop-down list" )
     */
    private $projectID;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50)
     * @Assert\Regex( pattern="/^([A-Z- ])+$/i",
     *                htmlPattern="([A-Za-z- ])+",
     *                message = "Invalid characters in company role" )
     * @Assert\NotBlank( message = "Role cannot be empty" )
     * @Assert\Length( max = 50, min = 3,
     *                 maxMessage = "Role cannot exceed {{ limit }} characters",
     *                 minMessage = "Role is under the minimum length of {{ limit }} characters")
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="investment", type="string", length=11, nullable=true)
     * @Assert\Length( max = 11,
     *                 maxMessage = "Investment cannot exceed {{ limit }} characters")
     * @Assert\Regex( pattern="/^[\d]+[.]?[\d]{1,2}$/",
     *                htmlPattern="[\d]+[.]?[\d]{1,2}",
     *                message = "Investment should only contain numeric characters" )
     */
    private $investment;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyID
     *
     * @param integer $companyID
     *
     * @return CompanyProjectRole
     */
    public function setCompanyID($companyID)
    {
        $this->companyID = $companyID;

        return $this;
    }

    /**
     * Get companyID
     *
     * @return int
     */
    public function getCompanyID()
    {
        return $this->companyID;
    }

    /**
     * Set projectID
     *
     * @param integer $projectID
     *
     * @return CompanyProjectRole
     */
    public function setProjectID($projectID)
    {
        $this->projectID = $projectID;

        return $this;
    }

    /**
     * Get projectID
     *
     * @return int
     */
    public function getProjectID()
    {
        return $this->projectID;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return CompanyProjectRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set investment
     *
     * @param string $investment
     *
     * @return CompanyProjectRole
     */
    public function setInvestment($investment)
    {
        $this->investment = $investment;

        return $this;
    }

    /**
     * Get investment
     *
     * @return string
     */
    public function getInvestment()
    {
        return $this->investment;
    }
}


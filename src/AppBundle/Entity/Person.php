<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=35)
     * @Assert\NotBlank(
     *      message="Please enter a first name."
     * )@Assert\NotBlank()
     * @Assert\Length(
     *      max = 35,
     *      maxMessage = "First name cannot exceed 35 characters."
     * )
     * @Assert\Regex(
     *      pattern="/[~!@#$%^&*()_+\={}\]\["";:\\?>.<,|\/]/",
     *      match=false,
     *      message="First name cannot contain invalid characters."
     * )
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=35)
     * @Assert\NotBlank(
     *      message="Please enter a last name."
     * )
     * @Assert\Length(
     *      max = 35,
     *      maxMessage = "Last name cannot exceed 35 characters."
     * )
     * @Assert\Regex(
     *      pattern="/[~!@#$%^&*()_+\={}\]\["";:\\?>.<,|\/]/",
     *      match=false,
     *      message="Last name cannot contain invalid characters."
     * )
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=15, nullable=true)
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "Please enter a valid Phone number (too short).",
     *      maxMessage = "Please enter a valid Phone number (too long)."
     * )
     * @Assert\Regex(
     *      pattern="/^\d*$/",
     *      match=true,
     *      message="Phone number cannot contain anything but numbers."
     * )
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Please enter a valid description (too short).",
     *      maxMessage = "Please enter a valid description (too long)."
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @Assert\Length(
     *      min = 7,
     *      max = 255,
     *      minMessage = "Please enter a valid Email (too short).",
     *      maxMessage = "Please enter a valid Email (too long)."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="string", length=255, nullable=true)
     */
    private $availability;

    /**
     * @var string
     *
     * @ORM\Column(name="specialty", type="string", length=255, nullable=true)
     */
    private $specialty;

    /**
     * @var string
     *
     * @ORM\Column(name="specialtyDescription", type="string", length=255, nullable=true)
     */
    private $specialtyDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="skill", type="string", length=255, nullable=true)
     */
    private $skill;

    /**
     * @var string
     *
     * @ORM\Column(name="skillDescription", type="string", length=255, nullable=true)
     */
    private $skillDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="researcherLevel", type="string", length=255, nullable=true)
     */
    private $researcherLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="researcherPayCategory", type="string", length=255, nullable=true)
     */
    private $researcherPayCategory;

    /**
     * @var int
     *
     * @ORM\Column(name="userID", type="integer", nullable=true, unique=true)
     */
    private $userID;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Person
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Person
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Person
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set availability
     *
     * @param string $availability
     *
     * @return Person
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Set specialty
     *
     * @param string $specialty
     *
     * @return Person
     */
    public function setSpecialty($specialty)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return string
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Set specialtyDescription
     *
     * @param string $specialtyDescription
     *
     * @return Person
     */
    public function setSpecialtyDescription($specialtyDescription)
    {
        $this->specialtyDescription = $specialtyDescription;

        return $this;
    }

    /**
     * Get specialtyDescription
     *
     * @return string
     */
    public function getSpecialtyDescription()
    {
        return $this->specialtyDescription;
    }

    /**
     * Set skill
     *
     * @param string $skill
     *
     * @return Person
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Get skill
     *
     * @return string
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set skillDescription
     *
     * @param string $skillDescription
     *
     * @return Person
     */
    public function setSkillDescription($skillDescription)
    {
        $this->skillDescription = $skillDescription;

        return $this;
    }

    /**
     * Get skillDescription
     *
     * @return string
     */
    public function getSkillDescription()
    {
        return $this->skillDescription;
    }

    /**
     * Set researcherLevel
     *
     * @param string $researcherLevel
     *
     * @return Person
     */
    public function setResearcherLevel($researcherLevel)
    {
        $this->researcherLevel = $researcherLevel;

        return $this;
    }

    /**
     * Get researcherLevel
     *
     * @return string
     */
    public function getResearcherLevel()
    {
        return $this->researcherLevel;
    }

    /**
     * Set researcherPayCategory
     *
     * @param string $researcherPayCategory
     *
     * @return Person
     */
    public function setResearcherPayCategory($researcherPayCategory)
    {
        $this->researcherPayCategory = $researcherPayCategory;

        return $this;
    }

    /**
     * Get researcherPayCategory
     *
     * @return string
     */
    public function getResearcherPayCategory()
    {
        return $this->researcherPayCategory;
    }

    /**
     * Set userID
     *
     * @param integer $userID
     *
     * @return Person
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;

        return $this;
    }

    /**
     * Get userID
     *
     * @return int
     */
    public function getUserID()
    {
        return $this->userID;
    }

    public function __toString()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }
}


<?php

/**
 * Create the Project Entity
 *
 * @version 1.0
 * @authors cst222 and cst216
 */
namespace AppBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="Project")
 */
class Project
{
    //Attributes
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="projectID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, name="projectName")
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9 ]*$/", match=true)
     */
    private $projectName;

    /**
     * @var string
     * @ORM\Column(type="string", length=500, nullable=true, name="description")
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", name="startDate")
     * @Assert\Range(
     *      min = "-1 weeks",
     *      max = "+1 weeks"
     * )
     * @Assert\NotBlank()
     */
    private $startDate;

    /**
     * @ORM\Column(type="string", nullable=true, name="endDate")
     * @Assert\Date()
     */
    private $endDate;

    /**
     * @var double
     * @ORM\Column(type="float", name="budget")
     */
    private $budget;

    /**
     * @var string
     * @ORM\Column(type="string", name="status")
     * @Assert\Choice(
     *      choices = {"Pre-Project", "Proposal", "Submitted", "Accepted", "In Progress", "Completed", "On Hold"},
     *      message = "Please select a valid status."
     * )
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="keywords")
     */
    private $keywords;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="media")
     */
    private $media;

    //Constructor
    //public function __construct($name, $budget=0.0, $description="", $keywords="", $media="")
    //{
    //    $this->projectName = $name;
    //    $this->startDate =new DateTime('now');//date('Y-m-d');
    //    $this->status = "In Progress";
    //    $this->endDate = "";
    //    $this->budget = $budget;
    //    $this->description = $description;
    //    $this->keywords = $keywords;
    //    $this->media = $media;
    //}

    //Methods

    #region Validation Methods
    /**
     * Check validity of project name, should be between 5 and 50
     * characters and should not contain any special characters
     */
    //public function isValidProjectName()
    //{
    //    $excludeChars = '/[\'\/~`\!@#\$%\^&\*_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';

    //    return !empty(trim($this->projectName)) && (mb_check_encoding($this->projectName, 'UTF-8') && (strlen($this->projectName) <= 50) && !preg_match($excludeChars, $this->projectName));
    //}

    ///**
    // * Check if start date is within a valid range, a valid range is
    // * a week difference from current date
    // */
    //public function isValidStartDate()
    //{
    //    return ($this->startDate < date('d/m/Y', strtotime("-1 week")) || $this->startDate > date('d/m/Y', strtotime("+1 week")));
    //}

    //public function isProjectFormValid()
    //{
    //    return isValidProjectName() && isValidStartDate();
    //}

    #endregion

    #region Getters & Setters
    /**
     * Gets the project id for this project
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * Gets the project name for this project
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Sets the project name for this project
     * @param mixed $name
     */
    public function setProjectName($name)
    {
    	$this->projectName = $name;
    }

    /**
     * Gets the description for this project
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description for this project
     * @param mixed $description
     */
    public function setDescription($description)
    {
    	$this->description = $description;
    }

    /**
     * Sets the start date for this project
     * @param mixed $startDate
     */
    public function setStartDate ($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Gets the start date from this project
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the end date for this project
     * @param mixed $endDate
     */
    public function setEndDate ($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Gets the end date from this project
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Gets the budget from this project
     * @return double
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Sets the budget for this project
     * @param mixed $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * Gets the keywords for this project
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the keywords for this project
     * @param mixed $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * Gets the media for this project
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Sets the media for this project
     * @param mixed $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }

    /**
     * Gets the status for this project
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status for this project
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    #endregion
	
    public function __toString()
    {
        return $this->getProjectName();
    }
}
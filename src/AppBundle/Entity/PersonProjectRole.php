<?php

namespace AppBundle\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * PersonProjectRole
 *
 * @ORM\Table(name="person_project_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonProjectRoleRepository")
 * @UniqueEntity(fields = {"personID", "projectID", "role"}, message="This relation already Exists")
 */
class PersonProjectRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     *  @ORM\ManyToOne(targetEntity="Person", inversedBy="person")
     *  @ORM\JoinColumn(name="personID", referencedColumnName="id")
     *  @Assert\NotBlank(message="Please select a Person")
     */
    private $personID;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="project")
     * @ORM\JoinColumn(name="projectID", referencedColumnName="projectID")
     * @Assert\NotBlank(message="Please select a Project")
     */
    private $projectID;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     * @Assert\NotBlank(message="Please select a Role")
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "The role is too short.",
     *      maxMessage = "The role is too long."
     * )
     */
    private $role;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personID
     *
     * @param $personID
     *
     * @return PersonProjectRole
     */
    public function setPersonID($personID)
    {
        $this->personID = $personID;

        return $this;
    }

    /**
     * Get personID
     *
     * @return int
     */
    public function getPersonID()
    {
        return $this->personID;
    }

    /**
     * Set projectID
     *
     * @param $projectID
     *
     * @return PersonProjectRole
     */
    public function setProjectID($projectID)
    {
        $this->projectID = $projectID;

        return $this;
    }

    /**
     * Get projectID
     *
     * @return int
     */
    public function getProjectID()
    {
        return $this->projectID;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return PersonProjectRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }


}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PersonCompanyRole
 *
 * @ORM\Table(name="person_company_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonCompanyRoleRepository")
 */
class PersonCompanyRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * 
     * @Assert\NotBlank( message = "Please select a valid person." )
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="person")
     * @ORM\JoinColumn(name="personID", referencedColumnName="id")
     */
    private $personID;

    /**
     * @var int
     *
     * @Assert\NotBlank( message = "Please select a valid company." )
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="company")
     * @ORM\JoinColumn(name="companyID", referencedColumnName="id")
     */
    private $companyID;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50)
     * @Assert\NotBlank( message = "The role field is required." )
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Role must have at least 2 characters.",
     *      maxMessage = "The role field must be 50 characters or less."
     * )
     */
    private $role;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personID
     *
     * @param integer $personID
     *
     * @return PersonCompanyRole
     */
    public function setPersonID($personID)
    {
        $this->personID = $personID;

        return $this;
    }

    /**
     * Get personID
     *
     * @return int
     */
    public function getPersonID()
    {
        return $this->personID;
    }

    /**
     * Set companyID
     *
     * @param integer $companyID
     *
     * @return PersonCompanyRole
     */
    public function setCompanyID($companyID)
    {
        $this->companyID = $companyID;

        return $this;
    }

    /**
     * Get companyID
     *
     * @return int
     */
    public function getCompanyID()
    {
        return $this->companyID;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return PersonCompanyRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
}


<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompanyProjectRole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Companyprojectrole controller.
 *
 * @Route("companyprojectrole")
 */
class CompanyProjectRoleController extends Controller
{
    /**
     * Lists all companyProjectRole entities.
     *
     * @Route("/", name="companyprojectrole_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $companyProjectRoles = $em->getRepository('AppBundle:CompanyProjectRole')->findAll();

        return $this->render('companyprojectrole/index.html.twig', array(
            'companyProjectRoles' => $companyProjectRoles,
        ));
    }

    /**
     * Creates a new companyProjectRole entity.
     *
     * @Route("/new", name="companyprojectrole_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $companyProjectRole = new Companyprojectrole();
        $form = $this->createForm('AppBundle\Form\CompanyProjectRoleType', $companyProjectRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($companyProjectRole);
            $em->flush();

            return $this->redirectToRoute('companyprojectrole_show', array('id' => $companyProjectRole->getId()));
        }

        return $this->render('companyprojectrole/new.html.twig', array(
            'companyProjectRole' => $companyProjectRole,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a companyProjectRole entity.
     *
     * @Route("/{id}", name="companyprojectrole_show")
     * @Method("GET")
     */
    public function showAction(CompanyProjectRole $companyProjectRole)
    {
        $deleteForm = $this->createDeleteForm($companyProjectRole);

        return $this->render('companyprojectrole/show.html.twig', array(
            'companyProjectRole' => $companyProjectRole,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing companyProjectRole entity.
     *
     * @Route("/{id}/edit", name="companyprojectrole_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CompanyProjectRole $companyProjectRole)
    {
        $deleteForm = $this->createDeleteForm($companyProjectRole);
        $editForm = $this->createForm('AppBundle\Form\CompanyProjectRoleType', $companyProjectRole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('companyprojectrole_edit', array('id' => $companyProjectRole->getId()));
        }

        return $this->render('companyprojectrole/edit.html.twig', array(
            'companyProjectRole' => $companyProjectRole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a companyProjectRole entity.
     *
     * @Route("/{id}", name="companyprojectrole_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CompanyProjectRole $companyProjectRole)
    {
        $form = $this->createDeleteForm($companyProjectRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($companyProjectRole);
            $em->flush();
        }

        return $this->redirectToRoute('companyprojectrole_index');
    }

    /**
     * Creates a form to delete a companyProjectRole entity.
     *
     * @param CompanyProjectRole $companyProjectRole The companyProjectRole entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CompanyProjectRole $companyProjectRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('companyprojectrole_delete', array('id' => $companyProjectRole->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

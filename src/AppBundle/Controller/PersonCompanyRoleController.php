<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PersonCompanyRole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Personcompanyrole controller.
 *
 * @Route("personcompanyrole")
 */
class PersonCompanyRoleController extends Controller
{
    /**
     * Lists all personCompanyRole entities.
     *
     * @Route("/", name="personcompanyrole_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $personCompanyRoles = $em->getRepository('AppBundle:PersonCompanyRole')->findAll();

        return $this->render('personcompanyrole/index.html.twig', array(
            'personCompanyRoles' => $personCompanyRoles,
        ));
    }

    /**
     * Creates a new personCompanyRole entity.
     *
     * @Route("/new", name="personcompanyrole_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $personCompanyRole = new Personcompanyrole();
        $form = $this->createForm('AppBundle\Form\PersonCompanyRoleType', $personCompanyRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personCompanyRole);
            $em->flush();

            return $this->redirectToRoute('personcompanyrole_show', array('id' => $personCompanyRole->getId()));
        }

        return $this->render('personcompanyrole/new.html.twig', array(
            'personCompanyRole' => $personCompanyRole,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a personCompanyRole entity.
     *
     * @Route("/{id}", name="personcompanyrole_show")
     * @Method("GET")
     */
    public function showAction(PersonCompanyRole $personCompanyRole)
    {
        $deleteForm = $this->createDeleteForm($personCompanyRole);

        return $this->render('personcompanyrole/show.html.twig', array(
            'personCompanyRole' => $personCompanyRole,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing personCompanyRole entity.
     *
     * @Route("/{id}/edit", name="personcompanyrole_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PersonCompanyRole $personCompanyRole)
    {
        $deleteForm = $this->createDeleteForm($personCompanyRole);
        $editForm = $this->createForm('AppBundle\Form\PersonCompanyRoleType', $personCompanyRole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('personcompanyrole_edit', array('id' => $personCompanyRole->getId()));
        }

        return $this->render('personcompanyrole/edit.html.twig', array(
            'personCompanyRole' => $personCompanyRole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a personCompanyRole entity.
     *
     * @Route("/{id}", name="personcompanyrole_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PersonCompanyRole $personCompanyRole)
    {
        $form = $this->createDeleteForm($personCompanyRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personCompanyRole);
            $em->flush();
        }

        return $this->redirectToRoute('personcompanyrole_index');
    }

    /**
     * Creates a form to delete a personCompanyRole entity.
     *
     * @param PersonCompanyRole $personCompanyRole The personCompanyRole entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PersonCompanyRole $personCompanyRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personcompanyrole_delete', array('id' => $personCompanyRole->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

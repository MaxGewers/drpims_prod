<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PersonProjectRole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Personprojectrole controller.
 *
 * @Route("personprojectrole")
 */
class PersonProjectRoleController extends Controller
{
    /**
     * Lists all personProjectRole entities.
     *
     * @Route("/", name="personprojectrole_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $personProjectRoles = $em->getRepository('AppBundle:PersonProjectRole')->findAll();

        return $this->render('personprojectrole/index.html.twig', array(
            'personProjectRoles' => $personProjectRoles,
        ));
    }

    /**
     * Creates a new personProjectRole entity.
     *
     * @Route("/new", name="personprojectrole_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $personProjectRole = new Personprojectrole();
        $form = $this->createForm('AppBundle\Form\PersonProjectRoleType', $personProjectRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personProjectRole);
            $em->flush();

            return $this->redirectToRoute('personprojectrole_show', array('id' => $personProjectRole->getId()));
        }

        return $this->render('personprojectrole/new.html.twig', array(
            'personProjectRole' => $personProjectRole,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a personProjectRole entity.
     *
     * @Route("/{id}", name="personprojectrole_show")
     * @Method("GET")
     */
    public function showAction(PersonProjectRole $personProjectRole)
    {
        $deleteForm = $this->createDeleteForm($personProjectRole);

        return $this->render('personprojectrole/show.html.twig', array(
            'personProjectRole' => $personProjectRole,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing personProjectRole entity.
     *
     * @Route("/{id}/edit", name="personprojectrole_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PersonProjectRole $personProjectRole)
    {
        $deleteForm = $this->createDeleteForm($personProjectRole);
        $editForm = $this->createForm('AppBundle\Form\PersonProjectRoleType', $personProjectRole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('personprojectrole_edit', array('id' => $personProjectRole->getId()));
        }

        return $this->render('personprojectrole/edit.html.twig', array(
            'personProjectRole' => $personProjectRole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a personProjectRole entity.
     *
     * @Route("/{id}", name="personprojectrole_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PersonProjectRole $personProjectRole)
    {
        $form = $this->createDeleteForm($personProjectRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personProjectRole);
            $em->flush();
        }

        return $this->redirectToRoute('personprojectrole_index');
    }

    /**
     * Creates a form to delete a personProjectRole entity.
     *
     * @param PersonProjectRole $personProjectRole The personProjectRole entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PersonProjectRole $personProjectRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personprojectrole_delete', array('id' => $personProjectRole->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

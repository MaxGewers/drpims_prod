<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * story18Fixtures short summary.
 *
 * story18Fixtures description.
 *
 * @version 1.0
 * @author cst207
 */
class story18Fixtures extends Fixture
{

    // ...
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load (ObjectManager $manager)
    {

        $user = new User();
        $user->setUsername("TerryP");
        $user->setUsernameCanonical("terryp");
        $user->setEmail("terryp@fake.com");
        $user->setEmailCanonical("terryp@fake.com");
        $user->setEnabled(true);
        $password = $this->encoder->encodePassword($user, 'TestPassword');
        $user->setPassword($password);
        $user->setRoles(array());

        $manager->persist($user);

        $manager->flush();
    }

}
<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * story17_1Fixtures short summary.
 *
 * story17_1Fixtures description.
 *
 * @version 1.0
 * @author cst207
 */
class story17_1Fixtures extends Fixture
{

    private $company;

    public function load (ObjectManager $manager)
    {
        $this->company = new Company();
        $this->company->setName('Stealth');
        $this->company->setCountry("Canada");
        $this->company->setProvince("Saskatchewan");

        $manager->persist($this->company);

        $manager->flush();
    }
}
<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Person;

/**
 * story42Fixtures short summary.
 *
 * story42Fixtures description.
 *
 * @version 1.0
 * @author cst239
 */
class story42Fixtures extends Fixture
{
    private $person;
    private $person2;
    private $company;

    public function load (ObjectManager $manager)
    {
        $this->person = new Person();
        $this->person->setFirstName('John');
        $this->person->setLastName('Smith');
        $this->person->setEmail('js1234@gmail.com');
        $this->person->setPhoneNumber('3065555555');
        $this->person->setDescription('CST student');

        $manager->persist($this->person);

        $this->person2 = new Person();
        $this->person2->setFirstName('Cyril');
        $this->person2->setLastName('Coupal');
        $this->person2->setEmail('cyril_c@gmail.com');
        $this->person2->setPhoneNumber('3065555555');
        $this->person2->setDescription('Applied Researcher');

        $manager->persist($this->person2);

        $this->company = new Company();
        $this->company->setName('SGI');
        $this->company->setCountry("Canada");
        $this->company->setProvince("Saskatchewan");

        $manager->persist($this->company);

        $manager->flush();


    }

}
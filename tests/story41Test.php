<?php
namespace Tests;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\CompanyProjectRole;
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Unit testing for story 8: create a new compproj entity
 */
class story41Test extends KernelTestCase
{
    private $validator;
    private $compproj;

    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->compproj = new CompanyProjectRole();
        $this->compproj->setCompanyID(1);
        $this->compproj->setProjectID(1);
        $this->compproj->setRole('partner');
        $this->compproj->setInvestment('10000');
    }

    /*Company-Project Valid Input (Example Entry Test)*/
    public function testCompProjDefaultInfoSuccess()
    {
        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue(count($errors) < 1);
    }

    /*Company-Project Invalid Company ID (null)*/
    public function testCompProjCompanyIDNullFail()
    {
        $this->compproj->setCompanyID(null);

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Please select a valid company from the drop-down list');
    }

    /*Company-Project Role Invalid Input length of greater than 50 characters*/
    public function testCompProjRoleTooLongFail ()
    {
        //string repeat function
        $this->compproj->setRole(str_repeat('A', 51));

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Role cannot exceed 50 characters');
    }

    /*Company-Project Role Valid Input length of 50 characters*/
    public function testCompProjRoleMaxLengthSuccess ()
    {
        //string repeat function
        $this->compproj->setRole(str_repeat('A', 50));

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue(count($errors) < 1);
    }

    /*Company-Project Role Invalid Input length of 2 characters*/
    public function testCompProjRoleTooShortFail ()
    {
        //string repeat function
        $this->compproj->setRole('AA');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Role is under the minimum length of 3 characters');
    }

    /*Company-Project Role Empty*/
    public function testCompProjRoleEmptyFail ()
    {
        //string repeat function
        $this->compproj->setRole('');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Role cannot be empty');
    }

    /*Company-Project Role valid Input length of 3 characters*/
    public function testCompProjRoleMinLengthSuccess ()
    {
        //string repeat function
        $this->compproj->setRole('AAA');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue(count($errors) < 1);
    }

    /*Company-Project investment valid empty input*/
    public function testCompProjInvestmentEmptyPass ()
    {
        //string repeat function
        $this->compproj->setInvestment('');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue(count($errors) < 1);
    }

    /*Company-Project investment invalid character input*/
    public function testCompProjInvestmentInvalidCharsFail ()
    {
        //string repeat function
        $this->compproj->setInvestment('$10,000CAD');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Investment should only contain numeric characters');
    }

    /*Company-Project investment invalid character input*/
    public function testCompProjInvestmentInvalidLengthFail ()
    {
        //string repeat function
        $this->compproj->setInvestment('100000000000');

        $errors = $this->validator->validate($this->compproj);

        $this->assertTrue($errors[0]->getMessage() == 'Investment cannot exceed 11 characters');
    }
}


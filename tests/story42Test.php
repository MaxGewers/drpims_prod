<?php
namespace Tests;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Person;
use AppBundle\Entity\Company;
use AppBundle\Entity\PersonCompanyRole; 
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


/**
 * Unit tests to test the functionality for story 42. 
 *
 * @version 1.0
 * @author cst239
 */
class story42Test extends KernelTestCase
{
    private $validator;
    private $person;
    private $company;
    private $personCompanyRole;

    /*
     * Sets up the Person and company to test the functionality for story 42. 
     */
    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->person = new Person();
        $this->person->setFirstName('John');
        $this->person->setLastName('Smith');
        $this->person->setEmail('js1234@gmail.com');
        $this->person->setPhoneNumber('3065555555');
        $this->person->setDescription('CST student');

        $this->company = new Company();
        $this->company->setName('Company 1');
        $this->company->setCountry('Canada');
        $this->company->setProvince('SK');
        $this->company->setCity('Saskatoon');

        $this->personCompanyRole = new PersonCompanyRole();
        $this->personCompanyRole->setPersonID(1);
        $this->personCompanyRole->setCompanyID(1);
        $this->personCompanyRole->setRole('valid role');
    }

    /*
     * Tests that a valid personID is accepted. 
     */
    public function testPersonValid()
    {
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'personID');

        $this->assertTrue(count($errors) < 1);
    }

    /* 
     * Tests that an invalid personID is not accepted. 
     * */
    public function testPersonInvalid()
    {
        $this->personCompanyRole->setPersonID(null); //enter invalid personID
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'personID');
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'Please select a valid person.');

    }

    /**
     * Tests that a valid companyID is accepted. 
     */
    public function testCompanyValid()
    {
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'companyID');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests that an invalid company name is not accepted. 
     */
    public function testCompanyInvalid()
    {
        $this->personCompanyRole->setCompanyID(null); //enter invalid companyID
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'companyID');
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'Please select a valid company.');
    }

    /**
     * Tests that a role with a minimum length of 2 is accepted. 
     */
    public function testRoleMinLengthValid()
    {
        $this->personCompanyRole->setRole('CO');
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests that a role with less than 2 characters is not accepted. 
     */
    public function testRoleTooShortFail()
    {
        $this->personCompanyRole->setRole('A');
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'Role must have at least 2 characters.');

    }

    /**
     * Tests that a role with the maximum length of 50 characters is accepted. 
     */
    public function testRoleMaxLengthValid()
    {
        $this->personCompanyRole->setRole(str_repeat('A', 50));
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests that a role with a length of more than 50 characters is not accepted.
     */
    public function testRoleMaxLengthExceededFail()
    {
        $this->personCompanyRole->setRole(str_repeat('A', 51));
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'The role field must be 50 characters or less.');
    }

    /**
     * Tests that a valid role is accepted. 
     */
    public function testValidRoleSuccess()
    {
        $this->personCompanyRole->setRole('Employee');
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests that an empty role will not be accepted. 
     */
    public function testRoleNotFilledOutFail()
    {
        $this->personCompanyRole->setRole("");
        $errors = $this->validator->validateProperty($this->personCompanyRole, 'role');
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'The role field is required.');
    }

}
<?php
namespace Tests;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Person;
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Unit testing for story 8: create a new person entity
 */
class story8Test extends KernelTestCase
{
    private $validator;
    private $person;

    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->person = new Person();
        $this->person->setFirstName('John');
        $this->person->setLastName('Smith');
        $this->person->setEmail('js1234@gmail.com');
        $this->person->setPhoneNumber('3065555555');
        $this->person->setDescription('CST student');
    }

    /*Person First Name Valid Input (Example Entry Test)*/
    public function testPersonFirstNameSuccess()
    {
        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person First Name Invalid Input length of greater than 35 characters*/
    public function testPersonFirstNameTooLongFail ()
    {
        //string repeat function
        $this->person->setFirstName(str_repeat('A', 36));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) == 1);
        $this->assertTrue($errors[0]->getMessage() == 'First name cannot exceed 35 characters.');
    }

    /*Person First Name Invalid Input length of greater than 35 characters*/
    public function testPersonFirstNameMaxLengthSuccess ()
    {
        //string repeat function
        $this->person->setFirstName(str_repeat('A', 35));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person First Name Valid input length of 1 character*/
    public function testPersonFirstNameMinimumLengthSuccess ()
    {
        $this->person->setFirstName('A');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person First Name Invalid characters*/
    public function testPersonFirstNameInvalidCharactersFail ()
    {
        $this->person->setFirstName('John#$');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'First name cannot contain invalid characters.');
    }

    /*Person First Name Invalid Input empty*/
    public function testPersonFirstNameEmptyFail ()
    {
        $this->person->setFirstName('');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a first name.');
    }

    /*Person Last Name Valid Input (Example Entry Test)*/
    public function testPersonLastNameSuccess ()
    {
        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Last Name Invalid Input length of greater than 35 characters*/
    public function testPersonLastNameTooLongFail ()
    {
        //string repeat function
        $this->person->setLastName(str_repeat('A', 36));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Last name cannot exceed 35 characters.');
    }

    /*Person Last Name Invalid Input length of greater than 35 characters*/
    public function testPersonLastNameMaxLengthSuccess ()
    {
        //string repeat function
        $this->person->setLastName(str_repeat('A', 35));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Last Name Valid input length of 1 character*/
    public function testPersonLastNameMinimumLengthSuccess ()
    {
        $this->person->setLastName('A');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Last Name Invalid characters*/
    public function testPersonLastNameInvalidCharactersFail ()
    {
        $this->person->setLastName('Smith#$');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Last name cannot contain invalid characters.');
    }

    /*Person Last Name Invalid Input empty*/
    public function testPersonLastNameEmptyFail ()
    {
        $this->person->setLastName('');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a last name.');
    }

    /*Person Email is valid*/
    public function testPersonEmailValidPass ()
    {
        $this->person->setEmail('a@aa.com');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Email is invalid missing character*/
    public function testPersonEmailInvalidMissingCharacterFail ()
    {
        $this->person->setEmail('aaaaaAT.com');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == "The email '\"aaaaaAT.com\"' is not a valid email.");
        //The email \'\"aaaaaAT.com\"\' is not a valid email.
    }

    /*Person Email is invalid too long*/
    public function testPersonEmailInvalidMaxLengthFail ()
    {
        $this->person->setEmail(str_repeat('a',250).'@aa' .'.com');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a valid Email (too long).');
    }

    /*Person Email is valid max length*/
    public function testPersonEmailInvalidMaxLengthPass ()
    {
        $this->person->setEmail(str_repeat('a',248) . '@aa.com');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Email is valid min character*/
    public function testPersonEmailInvalidMinLengthPass ()
    {
        $this->person->setEmail('a@bb.ca');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Email is invalid min character (cant test)*/

    /*Person Phone is invalid too long*/
    public function testPersonPhoneMaxLengthFail ()
    {
        $this->person->setPhoneNumber('1030605550555512');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a valid Phone number (too long).');
    }

    /*Person Phone is valid Max character*/
    public function testPersonPhoneInvalidMaxLengthPass ()
    {
        $this->person->setPhoneNumber('10306055505555');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Phone is invalid min character*/
    public function testPersonPhonenvalidMinLengthFail ()
    {
        $this->person->setPhoneNumber('306123456');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == "Please enter a valid Phone number (too short).");

        //$this->assertTrue(strcmp($errors[0],'Please enter a valid Phone number (too short).') == 0);
    }

    /*Person Phone is valid min length*/
    public function testPersonPhoneValidMinLengthPass ()
    {
        $this->person->setPhoneNumber('3061234567');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person Phone contains invalid characters*/
    public function testPersonPhoneInvalidCharactersFail ()
    {
        $this->person->setPhoneNumber('306-123-4567');

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == "Phone number cannot contain anything but numbers.");
    }

    /*Person description is invalid too long*/
    public function testPersonDescriptionMaxLengthFail ()
    {
        $this->person->setDescription(str_repeat('a',256));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a valid description (too long).');
    }

    /*Person description is valid Max character*/
    public function testPersonDescriptionInvalidMinLengthPass ()
    {
        $this->person->setDescription(str_repeat('a',255));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }

    /*Person description is invalid min character*/
    public function testPersonDescriptionvalidMinLengthFail ()
    {
        $this->person->setDescription(str_repeat('a',2));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue($errors[0]->getMessage() == 'Please enter a valid description (too short).');
    }

    /*Person description is valid min length*/
    public function testPersonDescriptionInvalidMaxLengthPass ()
    {
        $this->person->setDescription(str_repeat('a',3));

        $errors = $this->validator->validate($this->person);

        $this->assertTrue(count($errors) < 1);
    }
}


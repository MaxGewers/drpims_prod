<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\Project;
use DateTime;


/**
 * Unit testing for story 16: As a researcher I want to change
 * the status of a project.
 *
 * @version 1.0
 * @author cst239
 */
class story16Test extends WebTestCase
{
    private $validator;
    private $project;

    /**
     * Sets up the project using a valid project name and start date. 
     * It sets the status to Pre-Project and the budget to 15000. 
     */
    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->project = new Project();
        $this->project->setProjectName('This is a Valid Project Name');
        $this->project->setStartDate(new DateTime('now'));
        $this->project->setBudget(15000.00);
        $this->project->setStatus('Pre-Project');
    }

    /**
     * Tests that a valid status input is accepted.
     */
    public function testProjectStatusChangeValid()
    {
        $this->project->setStatus('On Hold');
        $this->assertEquals('On Hold', $this->project->getStatus());
    }

    /**
     * Tests that an invalid status input is not accepted.
     */
    public function testProjectStatusInvalid()
    {
        $this->project->setStatus('status');
        $errors = $this->validator->validateProperty($this->project, 'status');
        //status change should not work
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'Please select a valid status.');
    }

    /**
     * Tests that a blank project status is not accepted.
     */
    public function testProjectStatusBlank()
    {
        $this->project->setStatus('');
        $errors = $this->validator->validateProperty($this->project, 'status');
        //status change should not work
        $this->assertTrue(count($errors) > 0);
        $this->assertTrue($errors[0]->getMessage() == 'Please select a valid status.');
    }

    /**
     * Tests that if the status is not changed on a newly created project,
     * it will have the status of "Pre-Project".
     */
    public function testProjectCreationDefaultStatus()
    {
        $this->assertEquals('Pre-Project', $this->project->getStatus());
    }

}
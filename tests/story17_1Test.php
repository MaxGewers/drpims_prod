<?php

namespace Tests;

use AppBundle\DataFixtures\story18Fixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use AppBundle\DataFixtures\ORM\story17_1Fixtures;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\Forms;
use Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;
use AppBundle\Entity\Company;

/**
 * To check Story 17.1
 *
 * @version 1.0
 * @author cst207 and cst230
 */
class story17_1Test extends WebTestCase
{
    private $validator;
    private $doc;
    private $companyDB;
    private $company;

    /**
     * Sets up the base information needed for all tests
     */
    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->doc = $kernal->getContainer()
        ->get('doctrine')
        ->getManager();

        $this->company = new Company();
        $this->company->setName('S');
        $this->company->setCountry("Canada");
        $this->company->setProvince("Saskatchewan");
        $this->company->setCity("Saskatoon");
        $this->company->setPostalCode("S0K");
        $this->company->setStreet('123 Sneaky Street');
        $this->company->setPhone('3068675309');
        $this->company->setWebsite('stealthmedia.com');
        $this->company->setDescription('A sneaky company.');

    }

    /**
     * Checks if the company name is validated successfully
     */
    public function testCompanyNameSuccessfulMin(){
        $errors = $this->validator->validateProperty($this->company, 'name');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Checks if the company name is validated successfully
     */
    public function testCompanyNameSuccessfulMax(){
        $this->company->setName(str_repeat('S', 255));
        $errors = $this->validator->validateProperty($this->company, 'name');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Checks if the company name is too long
     */
    public function testCompanyNameTooLongFail(){

        $this->company->setName(str_repeat('S', 256));

        $errors = $this->validator->validateProperty($this->company, 'name');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);

    }
    /**
     * Checks if the company name is empty
     */
    public function testCompanyNameEmptyFail(){

        $this->company->setName("");

        $errors = $this->validator->validateProperty($this->company, 'name');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "fill out" ) !== false);

    }

    /**
     * Tests if the country is validated successfully
     */
    public function testCountrySuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'country');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests if the country is empty
     */
    public function testCountryEmptyFail(){

        $this->company->setCountry("");

        $errors = $this->validator->validateProperty($this->company, 'country');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "fill out" ) !== false);
    }

    /**
     * Tests if the province is validated correctly
     */
    public function testProvinceSuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'province');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests if the province is empty
     */
    public function testProvinceEmptyFail(){

        $this->company->setProvince("");

        $errors = $this->validator->validateProperty($this->company, 'province');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "fill out" ) !== false);
    }

    /**
     * Checks if the City is validated successfully with 255 characters
     */
    public function testCitySuccessfulMax(){
        $str = str_repeat('s', 255);
        $this->company->setCity($str);
        $errors = $this->validator->validateProperty($this->company, 'city');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Checks if the City is too long with 256 characters
     */
    public function testCityTooLongFail(){

        $this->company->setCity(str_repeat('s', 256));

        $errors = $this->validator->validateProperty($this->company, 'city');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);

    }


    /**
     * Checks if the Street is validated successfully with 255 characters
     */
    public function testStreetSuccessfulMax(){
        $this->company->setStreet(str_repeat('s', 255));
        $errors = $this->validator->validateProperty($this->company, 'street');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Checks if the Street is too long with 256 characters
     */
    public function testStreetTooLongFail(){

        $this->company->setStreet(str_repeat('s', 256));

        $errors = $this->validator->validateProperty($this->company, 'street');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);

    }



    /**
     * Tests if the postal code is validated correctly
     */
    public function testPostalCodeSuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'postalCode');

        $this->assertTrue($errors == "");
    }

    /**
     * Tests if the postal code has invalid characters
     */
    public function testPostalCodeInvalidCharacterFail(){

        $this->company->setPostalCode("S@K 2T2");

        $errors = $this->validator->validateProperty($this->company, 'postalCode');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "Accepted values" ) !== false);
    }

    /**
     * Tests if the postal code has too many characters
     */
    public function testPostalCodeLongFail(){

        $this->company->setPostalCode("1-2-3-4-5-6-7-8-9-10-11");

        $errors = $this->validator->validateProperty($this->company, 'postalCode');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);
    }

    /**
     * Tests if the phone number is validated correctly
     */
    public function testPhoneSuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'phone');

        $this->assertTrue($errors == "");
    }

    /**
     * Tests if the phone number has invalid characters
     */
    public function testPhoneInvalidCharacterFail(){

        $this->company->setPhone("306-867-5309");

        $errors = $this->validator->validateProperty($this->company, 'phone');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "Accepted values" ) !== false);
    }

    /**
     * Tests if the phone number is too short
     */
    public function testPhoneShortFail(){

        $this->company->setPhone("8675309");

        $errors = $this->validator->validateProperty($this->company, 'phone');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "lengthen" ) !== false);
    }

    /**
     * Tests if the phone number is too long
     */
    public function testPhoneLongFail(){

        $this->company->setPhone("3068675309123456");

        $errors = $this->validator->validateProperty($this->company, 'phone');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);
    }

    /**
     * Tests if the website is validated correctly
     */
    public function testWebsiteSuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'website');

        $this->assertTrue($errors == "");
    }

    /**
     * Tests if the website has an invalid URL
     */
    public function testWebsiteInvalidURLFail(){

        $this->company->setWebsite("stealthmedia");

        $errors = $this->validator->validateProperty($this->company, 'website');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "valid" ) !== false);
    }


    /**
     * Tests if the website's url is too long at 256 characters
     */
    public function testWebsiteURLTooLongFail(){

        $this->company->setWebsite(str_repeat("stealthmedia.com",16));

        $errors = $this->validator->validateProperty($this->company, 'website');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);
    }


    /**
     * Tests if the description is validated correctly
     */
    public function testDescriptionSuccessful(){
        $errors = $this->validator->validateProperty($this->company, 'description');

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests if the description is too long at 1001 characters
     */
    public function testDescriptionTooLongFail(){

        $this->company->setDescription(str_repeat("E",1001));

        $errors = $this->validator->validateProperty($this->company, 'description');

        $this->assertTrue(count($errors) > 0);

        $this->assertTrue(strpos($errors, "shorten" ) !== false);
    }

    /**
     * Tests if all the company info is validated correctly
     */
    public function testCompanyAllInfoSuccessful(){
        $errors = $this->validator->validate($this->company);

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests if the only the minimum required inputs are filled out
     * that it will still be validated correctly
     */
    public function testCompanyMinInfoSuccessful(){

        $this->company->setCity("");
        $this->company->setPostalCode("");
        $this->company->setStreet('');
        $this->company->setPhone('');
        $this->company->setWebsite('');
        $this->company->setDescription('');

        $errors = $this->validator->validate($this->company);

        $this->assertTrue(count($errors) < 1);
    }

    /**
     * Tests that if a company already exists in the Database
     * that a new company is not created
     */
    public function testCompanyAlreadyExists(){
        $loader = new Loader();
        $loader->addFixture(new story17_1Fixtures);

        $purger = new ORMPurger($this->doc);
        $executor = new ORMExecutor($this->doc, $purger);
        $executor->execute($loader->getFixtures());

        $this->company->setName('Stealth');

        $errors = $this->validator->validate($this->company);

        $this->assertTrue(strpos($errors, "already used" ) !== false);
    }


    /**
     * Tests that a company is created successfully
     */
    public function testCompanyCreatedSuccessfully(){

        $loader = new Loader();
        $loader->addFixture(new story17_1Fixtures);

        $purger = new ORMPurger($this->doc);
        $executor = new ORMExecutor($this->doc, $purger);
        $executor->execute($loader->getFixtures());

        $this->company->setName('Stealth');
        $companyDB = $this->doc
            ->getRepository(Company::class)
            ->findByName($this->company->getName());

        $this->assertCount(1, $companyDB);

    }

}
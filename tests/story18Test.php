<?php

namespace Tests;

use AppBundle\DataFixtures\ORM\story18Fixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\Forms;
use Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;

#type
use FOS\UserBundle\Form\Type\UsernameFormType;
#model
use FOS\UserBundle\Model\User;

/**
 * To check Story 18
 *
 * @version 1.0
 * @author cst212 and CST207
 */
class story18Test extends WebTestCase
{


     private $doc;

    /**
     * Sets up the base information needed for all tests
     */
    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->doc = $kernal->getContainer()
        ->get('doctrine')
        ->getManager();
    }
    /**
     * Checks the login page HTTP code for 200 (avaliable)
     * and the redirects
     */
    public function testLoginPageAvaliability()
    {
        $client = static::createClient();
        $client->request('GET', "/");

        //Code 200 means the page exists and no errors
        $this->assertSame(
            200,
            $client->getResponse()->getStatusCode(),
            sprintf('The %s public URL loads correctly.', "projectList")
        );
    }

    /**
     * Checks that the login form is working and handing form request correctly
     * and the redirects
     */
    public function testLoginSucess()
    {
        $loader = new Loader();
        $kernal = self::bootKernel();
        $encoder = $kernal->getContainer()->get('security.password_encoder');
        $loader->addFixture(new story18Fixtures($encoder));

        $purger = new ORMPurger($this->doc);
        $executor = new ORMExecutor($this->doc, $purger);
        $executor->execute($loader->getFixtures());

        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        //fill out the form
        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = 'TerryP';
        $form['_password'] = 'TestPassword';
        $crawler = $client->submit($form);

        //checks that we get redirected and follows the redirect
        $this->assertTrue($client->getResponse()->isRedirect());
        $client->followRedirect();
        //Checks that text on the page exists bu looking for a text on it
        $this->assertContains(
        'Symfony 3.3.10',
        $client->getResponse()->getContent()
    );

    }

    /**
     * check that the login page is handeling incorrect usernames and passwords correctly
     * and the redirects
     */
    public function testLoginFail()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');


        $form = $crawler->selectButton('_submit')->form();
        $form['_username'] = 'fail';
        $form['_password'] = 'fail';
        $crawler = $client->submit($form);
        //Select and fill out the form
        $this->assertTrue($client->getResponse()->isRedirect());
        $client->followRedirect();
        //Checks that text on the page exists bu looking for a text on it
        $this->assertContains(
        'Invalid credentials.',
        $client->getResponse()->getContent()
        );
    }

    /**
     * Checks all of the function test for sotry 18 (all combinations of usernames and passwords)
     */
    public function testFunctionTests()
    {
        $loader = new Loader();
        $kernal = self::bootKernel();
        $encoder = $kernal->getContainer()->get('security.password_encoder');
        $loader->addFixture(new story18Fixtures($encoder));

        $purger = new ORMPurger($this->doc);
        $executor = new ORMExecutor($this->doc, $purger);
        $executor->execute($loader->getFixtures());

        //Dont do functional tests all in one test case. break them up.
        $loginCred = [
            ["TerryP", "TestPassword"],
            ["TerryG","TestPassword"],
            ["TerryP", "TestPasswordS"],
            ["TerryG","TestPasswordS"],
            ["","TestPassword"],
            ["TerryG",""],
            ["1234567890abcdefg","TestPassword"],
            ["TerryP","111111111111111111111111111111111111111111111111111"],
            ["12","TestPassword"],
            ["TerryP","123"]
        ];

        foreach ($loginCred as $item)
        {
            //First iten in the list asserts to True ( login should work )
            if ($loginCred[0] == $item)//first item passes
            {
                $client = static::createClient();
                $crawler = $client->request('GET', '/login');

                //Select and fill out the form
                $form = $crawler->selectButton('_submit')->form();
                $form['_username'] = $item[0];
                $form['_password'] = $item[1];
                $crawler = $client->submit($form);

                //checks that we get redirected and follows the redirect
                $this->assertTrue($client->getResponse()->isRedirect());
                $client->followRedirect();
                //Checks that text on the page exists
                $this->assertContains(
                'Symfony 3.3.10',
                $client->getResponse()->getContent()
                );
            }
            else //all other items in the list should assert to false (invalid Login)
            {
                $client = static::createClient();
                $crawler = $client->request('GET', '/login');

                //Select and fill out the form
                $form = $crawler->selectButton('_submit')->form();
                $form['_username'] = $item[0];
                $form['_password'] = $item[1];
                $crawler = $client->submit($form);

                $this->assertTrue($client->getResponse()->isRedirect());
                $client->followRedirect();
                $this->assertContains(
                'Invalid credentials.',
                $client->getResponse()->getContent()
                );
            }

        }

    }

}
<?php
namespace Tests;

use AppBundle\Entity\PersonProjectRole;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\Validation;
use AppBundle\DataFixtures\ORM\test\story32Fixtures;
use AppBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Unit Tests For story 32 As a researcher I want to add an existing user to an existing project
 *
 * @version 1.0
 * @author cst232
 */
class story32Test extends KernelTestCase
{
    public $relation;

    /**
     * this is the constructor to setup the arrays for the tests
     */
    public function setUp()
    {
        $kernal = self::bootKernel();
        $this->validator = $kernal->getContainer()->get('validator');
        $this->relation = new PersonProjectRole();
        $this->relation->setProjectID(1);
        $this->relation->setRole("Researcher");
        $this->relation->setPersonID(1);
    }


    /**
     * this tests if the Researcher can successfully add a user to a project
     */
    public function testSucsesfullRelation()
    {
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) < 1); //check error messages for future tests and compare
    }

    /**
     * This tests if the Researcher can create a PersonProjectRole with out a role (which shouldn't work)
     */
    public function testBlankRoleSubmission()
    {
        $this->relation->setRole("");
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
        $this->assertTrue($errors[0]->getMessage() == 'Please select a Role');
    }

    /**
     * This tests if the Researcher can create a PersonProjectRole with out a user (which shouldn't work)
     */
    public function testBlankPersonSubmission()
    {
        $this->relation->setPersonID(null);
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
        $this->assertTrue($errors[0]->getMessage() == 'Please select a Person');
    }

    /**
     * This tests if the Researcher can create a PersonProjectRole with out a user (which shouldn't work)
     */
    public function testBlankProjectSubmission()
    {
        $this->relation->setProjectID(null);
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
        $this->assertTrue($errors[0]->getMessage() == 'Please select a Project');
    }

    ///**
    // * This tests if the Researcher can create a duplicate PersonProjectRole (which shouldn't work)
    // */
    //public function testExistingRelation()
    //{
    //    $this->relation->setPersonID(1);
    //    $this->relation->setProjectID(1);
    //    $this->relation->setRole("Researcher");

    //    $errors = $this->validator->validate($this->relation);
    //    $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
    //    $this->assertTrue($errors[0]->getMessage() == 'The relation already exists');
    //}

    ///**
    // * This tests if the Researcher can create a PersonProjectRole whith the same ProjectID and PerssonID but with a different Role
    // */
    //public function testExistingRelationWhithDifrentRole()
    //{
    //    $this->relation->setUser(1);
    //    $this->relation->setProject(1);
    //    $this->relation->setRole("Admin");

    //    $errors = $this->validator->validate($this->relation);
    //    $this->assertTrue(count($errors) == 0); //check error messages for future tests and compare
    //}

    ///**
    // * This tests if the Researcher can create a PersonProjectRole whith a User that isnt in the db
    // */
    //public function testtNonexistentUser()
    //{
    //    $this->relation->setPersonID(-1);
    //    $errors = $this->validator->validate($this->relation);
    //    $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
    //    $this->assertTrue($errors[0]->getMessage() == 'The user does not exist');
    //}

    /**
     * This tests the maximum character length of the role TEXT field
     */
    public function testTooManyCharactersRole()
    {
        $this->relation->setRole(str_repeat("a", 256));
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
        $this->assertTrue($errors[0]->getMessage() == 'The role is too long.');
    }

    /**
     * This tests the minimum character length of the role TEXT field
     */
    public function testTooFewCharactersRole()
    {
        $this->relation->setRole("a");
        $errors = $this->validator->validate($this->relation);
        $this->assertTrue(count($errors) == 1); //check error messages for future tests and compare
        $this->assertTrue($errors[0]->getMessage() == 'The role is too short.');
    }
}

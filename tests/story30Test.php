<?php
namespace Tests;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Project;
use DateTime;
use Symfony\Component\Validator\Validation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Unit testing for story 30: create a new empty project
 */
class story30Test extends KernelTestCase
{
    private $validator;
    private $project;

    protected function setUp()
    {
        $kernal = self::bootKernel();

        $this->validator = $kernal->getContainer()->get('validator');

        $this->project = new Project();
        $this->project->setProjectName('This is a Valid Project Name');
        $this->project->setStartDate(new DateTime('now'));
        $this->project->setBudget(15000.00);
        $this->project->setStatus('In Progress');
    }

    /*Project Name Successful Input*/
    public function testProjectNameSuccessful ()
    {
        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) < 1); //check error messages for future tests and compare
    }

    /*Project Name Unsuccessful Input length of greater than 50 characters*/
    public function testProjectNameInvalidLengthFail ()
    {
        //string repeat function
        $this->project->setProjectName(str_repeat('a', 51));

        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) > 0); //check error messages for future tests and compare
    }

    /*Project Name Unsuccessful Input invalid characters lower case letters, upper ccase letters, numbers and space characters*/
    public function testProjectNameInvalidCharsFail ()
    {
        $this->project->setProjectName("??This Project Name Contains Invalid Charcters%&");

        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) > 0); //check error messages for future tests and compare
    }

    /*Project Name Unsuccessful Input empty*/
    public function testProjectNameEmptyFail ()
    {
        $this->project->setProjectName("");

        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) > 0); //check error messages for future tests and compare
    }

    /*Project Date Successful within 1 week before or after current date*/
    public function testStartDateSuccess ()
    {
        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) < 1); //check error messages for future tests and compare
    }

    /*Project Date Unsuccessful*/
    public function testStartDateOutOfRangeFail ()
    {
        $this->project->setStartDate('2017-OCT-23');

        $errors = $this->validator->validate($this->project);

        $this->assertTrue(count($errors) > 0); //check error messages for future tests and compare
    }
}

